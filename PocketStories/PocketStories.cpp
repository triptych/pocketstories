// PocketStories.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "PassageManager.h"

int main()
{
	std::unique_ptr<PassageManager> _PassageManager = std::make_unique<PassageManager>("Data/data.json");

	if (_PassageManager)
	{
		if (_PassageManager->Init())
			_PassageManager->Start();
	}

	std::cout << "PocketNarrator: Thus concludes our story. The game is over, buddy! \n";
	system("pause");

	return 0;
}
