#include "pch.h"
#include "Functions.h"
#include <algorithm>

void Functions::ToLower(std::string& inString)
{
	std::for_each(inString.begin(), inString.end(), [](char& c) {
		c = ::tolower(c);
	});
}
