#pragma once
#include <string>
#include <vector>
#include <memory>
#include "nlohmann/json.hpp"

using json = nlohmann::json;

struct Link
{
public:

	const bool Parse(const json& data, std::string& error);
	std::string& GetText() { return Text; }
	const int& GetTargetPid() { return TargetPid; }

private:
	std::string Target = "";
	std::string Text = "";
	int TargetPid = 0;
};

class Passage
{
public:

	const bool Parse(const json& data, std::string& error);
	std::string& GetText() { return Text; }
	std::string DisplayLinks();
	bool GetTargetPidForSelection(const int& selection, int& currentPid);
	int GetPid() { return Pid; }

private:
	std::string Text = "";
	std::vector<std::shared_ptr<Link>> Links;
	int Pid = 0;
};