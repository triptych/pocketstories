#include "pch.h"
#include "Names.h"

static std::string private_PASSAGES_ROOT = "passages";
const std::string& PASSAGES_ROOT = private_PASSAGES_ROOT;

static std::string private_TEXT_ELEMENT = "text";
const std::string& TEXT_ELEMENT = private_TEXT_ELEMENT;

static std::string private_LINKS_ELEMENT = "links";
const std::string& LINKS_ELEMENT = private_LINKS_ELEMENT;

static std::string private_TARGET_ELEMENT = "target";
const std::string& TARGET_ELEMENT = private_TARGET_ELEMENT;

static std::string private_TARGETPID_ELEMENT = "targetpid";
const std::string& TARGETPID_ELEMENT = private_TARGETPID_ELEMENT;

static std::string private_ERROR_SELECTION = "PocketNarrator: I'm sorry, that option isn't available.";
const std::string& ERROR_SELECTION = private_ERROR_SELECTION;

static std::string private_ERROR_VALUE_NOT_FOUND = "VALUE NOT FOUND";
const std::string& ERROR_VALUE_NOT_FOUND = private_ERROR_VALUE_NOT_FOUND;