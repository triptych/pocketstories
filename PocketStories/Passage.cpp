#include "pch.h"
#include "Passage.h"
#include "Names.h"
#include "nlohmann/json.hpp"

const bool Passage::Parse(const json& data, std::string& error)
{
	extern const std::string& TEXT_ELEMENT;
	extern const std::string& LINKS_ELEMENT;
	extern const std::string& ERROR_VALUE_NOT_FOUND;

	Text = data.value(TEXT_ELEMENT, ERROR_VALUE_NOT_FOUND);
	if (Text.compare(ERROR_VALUE_NOT_FOUND) == 0)
	{
		error = "Error: text element missing for Passage \n" + data;
		return false;
	}

	if (data[LINKS_ELEMENT].is_array()) //Check to ensure Links element is an array of objects
	{
		if (data[LINKS_ELEMENT].size() >= 1)
		{
			for (size_t index = 0; index < data[LINKS_ELEMENT].size(); index++)
			{
				std::shared_ptr<Link> newLink = std::make_shared<Link>();
				if (!newLink->Parse(data[LINKS_ELEMENT][index], error))
				{
					return false;
				}
				Links.push_back(std::move(newLink));
			}
		}
	}
	else
	{
		error = "Error: links element missing or is not an array for Passage \n" + data;
		return false;
	}

	Pid = data.value("pid", -1); //Attempt to get Pid of Passage
	if (Pid == -1)
	{
		error = "Error: pid element missing Passage \n" + data;
		return false;
	}

	return true;
}

std::string Passage::DisplayLinks()
{
	std::string combinedLinks = "";
	for (int count = 0; count < Links.size(); count++)
	{
		combinedLinks = combinedLinks + std::to_string(count + 1) + ". " + Links[count]->GetText() + '\n';
	}
	return combinedLinks;
}

bool Passage::GetTargetPidForSelection(const int& selection, int& currentPid)
{
	if (selection < 1)
		return false;

	currentPid = Links[selection - 1]->GetTargetPid();
	return true;
}

const bool Link::Parse(const json& data, std::string& error)
{
	extern const std::string& TEXT_ELEMENT;
	extern const std::string& TARGET_ELEMENT;
	extern const std::string& TARGETPID_ELEMENT;
	extern const std::string& ERROR_VALUE_NOT_FOUND;
	
	Target = data.value(TARGET_ELEMENT, ERROR_VALUE_NOT_FOUND);
	if (Target.compare(ERROR_VALUE_NOT_FOUND) == 0)
	{
		error = "Error: target element missing for Link \n" + data;
	}
	Text = data.value(TEXT_ELEMENT, ERROR_VALUE_NOT_FOUND);
	if (Text.compare(ERROR_VALUE_NOT_FOUND) == 0)
	{
		error = "Error: text element missing for Link \n" + data;
	}
	TargetPid = data.value(TARGETPID_ELEMENT, -1);
	if (TargetPid == -1)
	{
		error = "Error: targetpid element missing for Link \n" + data;
	}

	return true;
}
